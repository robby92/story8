from django.shortcuts import render
import json
import requests
from django.http import JsonResponse


def home(request):
    return render(request, 'main/home.html')

def search(request):
    q = request.GET['q']
     
    # json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q)
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + q
    req = requests.get(url_tujuan)

    x = json.loads(req.content)

    return JsonResponse(x, safe=False)
