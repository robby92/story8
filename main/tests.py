from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from django.apps import apps
from .apps import MainConfig

# Create your tests here.
class Story8_test(TestCase):

    def test_page(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'main/home.html')
    
    def test_header(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello",html_response)

    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main')

    
