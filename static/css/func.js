$(document).ready(function() {
    $("#input").keyup( function() {
        var q = $("#input").val();
        // console.log(q);
        // var hasil = 'https://www.googleapis.com/books/v1/volumes?q=' + q;
        // console.log(hasil);
        $.ajax({
            url: data_books_url + q,
            success: function(data) {
                console.log(data);
                var hasil_data = $("#card");
                hasil_data.empty();
                for (i = 0; i < data.items.length; i++) {
                    var x = data.items[i].volumeInfo.title;
                    var y = data.items[i].volumeInfo.imageLinks.smallThumbnail;
                    var z = data.items[i].volumeInfo.authors;
                    var a = data.items[i].volumeInfo.publisher;
                    var b = data.items[i].volumeInfo.publishedDate;
                    hasil_data.append('<div class="card mb-3"style="max-width: 540px;">' +
                    '<div class="row no-gutters">' + '<div class="col-md-4">' + '<img '+'src="' + y + '"' + 'style="max-width: 100%;">'
                    + '</div>' + '<div class="col-md-8">' + '<div class="card-body">' + '<h5 class="card-title">' + x + '</h5>'
                    + '<p class="card-text">Authors : '+ z +'</p>' + '<p class="card-text">Publisher : '+ a +'</p>' + '<p class="card-text">Published Date : '+ b +'</p>');
                }
            }
        });
    });
});